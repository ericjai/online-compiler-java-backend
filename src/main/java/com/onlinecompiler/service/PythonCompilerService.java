package com.onlinecompiler.service;

public interface PythonCompilerService
{
	public String pythonInputFromFrontEnd(String input);

}
