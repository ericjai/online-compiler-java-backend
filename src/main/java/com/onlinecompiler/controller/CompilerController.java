package com.onlinecompiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinecompiler.service.CompilerService;

@RestController
@CrossOrigin
public class CompilerController
{
	@Autowired
	CompilerService compilerService;
	
	@PostMapping("/getinputfromfrontend")
	public String getInputFromFrontEnd(@RequestParam String editorinput)
	{		
		 return compilerService.getInputFromFrontEnd(editorinput);
		
	}	
	
	/*@PostMapping("/getinputfromfrontend")
	public void getInputFromFrontEnd(@RequestBody String editorinput)
	{		
		compilerService.getInputFromFrontEnd(editorinput);
		
	}	*/

}
