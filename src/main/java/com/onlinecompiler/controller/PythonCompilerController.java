package com.onlinecompiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinecompiler.service.CompilerService;
import com.onlinecompiler.service.PythonCompilerService;

@RestController
@CrossOrigin
public class PythonCompilerController
{
	@Autowired
	PythonCompilerService compilerService;
	
	@PostMapping("/pythoninput")
	public String getPythonInputFromFrontEnd(@RequestParam String editorinput)
	{		
		 return compilerService.pythonInputFromFrontEnd(editorinput);
		
	}	
	
	/*@PostMapping("/getinputfromfrontend")
	public void getInputFromFrontEnd(@RequestBody String editorinput)
	{		
		compilerService.getInputFromFrontEnd(editorinput);
		
	}	*/

}
