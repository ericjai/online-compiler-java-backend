package com.onlinecompiler.serviceimpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.CompilerService;
import com.onlinecompiler.service.CppCompilerService;

@Service
public class CppCompilerServiceImpl implements CppCompilerService {

	@Override
	public String cppInputFromFrontEnd(String input) {
		System.out.println(input);
		File file;
		File directory = new File("cppcompiler");
		try {
			if (directory.exists()) {
				file = new File("cppcompiler/source.cpp");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
				}
			else
			{
				directory.mkdir();
				file = new File("cppcompiler/source.cpp");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
			}
			System.out.println(directory.getAbsolutePath());
			ProcessBuilder builder = new ProcessBuilder("g++", directory.getAbsolutePath(),"/"+ "source.cpp" +  " -o " +  "output.exe");
			
			
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File("cppcompiler" + "/" + "error.log"));
			Process process = builder.start();
			Thread.sleep(2000);	
			
			String content = readFile(directory.getAbsolutePath()+ "/" + "error.log");
			if(content.toLowerCase().contains("exception")|| content.toLowerCase().contains("error"))
			{
				return content;
			}
			else
			{
				builder = new ProcessBuilder(directory.getAbsolutePath()+"/"+"output.exe");
				builder.redirectErrorStream(true);
				builder.redirectOutput(new File("cppcompiler" + "/" + "output.log"));
				process = builder.start();
				Thread.sleep(2000);
				String output=readFile("cppcompiler/output.log");
				return output;
			}
				 
			
			
		}catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	private String readFile(String filepath)
	{
		try {			
			BufferedReader reader = new BufferedReader(new FileReader(filepath));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			reader.close();
			return stringBuilder.toString();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	

}
