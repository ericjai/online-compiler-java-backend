package com.onlinecompiler.serviceimpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.CompilerService;

@Service
public class CompilerServiceImpl implements CompilerService {
	public String getInputFromFrontEnd(String input) {
		System.out.println(input);

		File file;
		File directory = new File("javacompiler");

		try {
			if (directory.exists()) {
				file = new File("javacompiler/source.java");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
			} else {
				directory.mkdir();
				file = new File("javacompiler/source.java");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
			}
			ProcessBuilder builder = new ProcessBuilder("javac", directory.getAbsolutePath() +"/" + "source.java");
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File("javacompiler" + "/" + "error.log"));
			Process process = builder.start();
			Thread.sleep(2000);			
			
			//reading error.log
			String content = readFile(directory.getAbsolutePath()+"/error.log");
			if(content.toLowerCase().contains("exception")|| content.toLowerCase().contains("error"))
			{
				return content;
			}
			else
			{
				builder = new ProcessBuilder("java", "-cp",directory.getAbsolutePath() +"/",  "Main");
				builder.redirectErrorStream(true);
				builder.redirectOutput(new File("javacompiler" + "/" + "output.log"));
				process = builder.start();
				Thread.sleep(2000);
				String output=readFile("javacompiler/output.log");
				return output;
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();

		}
		return null;
	}
	
	private String readFile(String filepath)
	{
		try {			
			BufferedReader reader = new BufferedReader(new FileReader(filepath));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			reader.close();
			return stringBuilder.toString();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
