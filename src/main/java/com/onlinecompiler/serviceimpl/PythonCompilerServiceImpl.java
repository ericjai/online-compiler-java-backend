package com.onlinecompiler.serviceimpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.CompilerService;
import com.onlinecompiler.service.PythonCompilerService;

@Service
public class PythonCompilerServiceImpl implements PythonCompilerService {
	
	@Override
	public String pythonInputFromFrontEnd(String input) {
		System.out.println(input);
		
		File file;
		File directory= new File("pythoncompiler");
		try {
			if (directory.exists()) {
				file = new File("pythoncompiler/pythonsource.py");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
			} else {
				directory.mkdir();
				file = new File("pythoncompiler/pythonsource.py");
				file.createNewFile();
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write(input);
				bw.close();
			}
			ProcessBuilder builder = new ProcessBuilder("python", directory.getAbsolutePath() +"/" + "pythonsource.py");
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File("pythoncompiler" + "/" + "python.log"));
			Process process = builder.start();
			Thread.sleep(2000);			
			
			//reading python.log file
			String content = readFile(directory.getAbsolutePath() + "/" + "python.log");			
			return content;
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();

		}
		return null;
	}
	
	private String readFile(String filepath)
	{
		try {			
			BufferedReader reader = new BufferedReader(new FileReader(filepath));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			reader.close();
			return stringBuilder.toString();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
